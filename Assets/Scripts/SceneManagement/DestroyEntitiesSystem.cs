using System;
using System.Linq;
using Entitas;

namespace SceneManagement
{
    public class DestroyEntitiesSystem : ITearDownSystem
    {
        void ITearDownSystem.TearDown()
        {
            var entities = Contexts.sharedInstance.game.GetEntities();
            foreach (var e in entities)
            {
                var componentIndexes = e.GetComponentIndices().ToList();
                var components = e.GetComponents().ToList();
                for (var i = 0; i < componentIndexes.Count; i++)
                {
                    if (HasGlobalAttribute(components[i]))
                        continue;
                    e.RemoveComponent(componentIndexes[i]);
                }

                if (e.GetComponents().Length == 0)
                    e.Destroy();
            }
        }
        
        private bool HasGlobalAttribute(object obj)
        {
            var t = obj.GetType();
            return Attribute.IsDefined(t, typeof(GlobalAttribute));
        }
    }
}
