using Entitas;
using UnityEngine.SceneManagement;

namespace SceneManagement
{
    public class LoadGameSceneSystem : IInitializeSystem
    {
        public void Initialize()
        {
            SceneManager.LoadScene(SceneNames.Game, LoadSceneMode.Additive);
        }
    }
}