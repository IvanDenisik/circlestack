namespace SceneManagement
{
    public static class SceneNames
    {
        public const string Global = "Global";
        public const string Game = "Game";
    }
}