using DataSaving;
using Entitas;
using UnityEngine;

namespace SceneManagement
{
    public class DebugButtonSystem : IInitializeSystem, IExecuteSystem
    {
        private bool clicked;
        private GameContext context;
        
        void IInitializeSystem.Initialize()
        {
            context = Contexts.sharedInstance.game;
            context.gameHierarchyHelper.instance.debugButton.onClick.AddListener(ButtonClicked);
        }

        private void ButtonClicked()
        {
            clicked = true;
        }
        
        void IExecuteSystem.Execute()
        {
            if (!clicked)
                return;
            clicked = false;
            
            context.CreateEntity().AddSaveData(new []{EnumDataType.Local});
            context.CreateEntity().AddRestartScene(new []{SceneNames.Game}, new []{SceneNames.Game});
        }
    }
}