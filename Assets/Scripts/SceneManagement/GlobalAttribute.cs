using System;

namespace SceneManagement
{
    [AttributeUsage(AttributeTargets.Class)]
    public class GlobalAttribute : System.Attribute
    {
    }
}