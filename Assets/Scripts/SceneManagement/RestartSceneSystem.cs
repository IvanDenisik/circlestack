using System.Collections.Generic;
using Entitas;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SceneManagement
{
    public class RestartSceneSystem : ICleanupSystem
    {
        private GameContext context;

        private string[] scenesToUnLoad;
        private string[] scenesToLoad;

        private List<string> unloadedScenes = new List<string>();
        
        public RestartSceneSystem()
        {
            context = Contexts.sharedInstance.game;
        }

        void ICleanupSystem.Cleanup()
        {
            if (!context.hasRestartScene)
                return;

            scenesToUnLoad = context.restartScene.scenesToUnload;
            scenesToLoad = context.restartScene.scenesToLoad;
            unloadedScenes.Clear();
            
            foreach(var sceneName in scenesToUnLoad)
            {
                var unloadOperation = SceneManager.UnloadSceneAsync(sceneName);
                unloadOperation.completed += operation => UnloadOperationCompleted(operation, sceneName);
            }
        }

        private void UnloadOperationCompleted(AsyncOperation operation, string sceneName)
        {
            unloadedScenes.Add(sceneName);

            if (unloadedScenes.Count < scenesToUnLoad.Length)
                return;
            
            foreach(var sceneToLoadName in scenesToLoad)
            {
                SceneManager.LoadScene(sceneToLoadName, LoadSceneMode.Additive);
            }
        }
    }
}