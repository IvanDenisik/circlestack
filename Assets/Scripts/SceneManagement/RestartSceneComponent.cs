using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace SceneManagement
{
    [Game, Unique, Cleanup(CleanupMode.DestroyEntity)]
    public class RestartSceneComponent : IComponent
    {
        public string[] scenesToUnload;
        public string[] scenesToLoad;
    }
}