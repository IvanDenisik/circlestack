using Entitas;
using System.Collections.Generic;
using Common;

namespace GameStage
{
    public class FinishGameSystem : ReactiveSystem<GameEntity>, IInitializeSystem
    {
        public FinishGameSystem() : base(Contexts.sharedInstance.game)
        {

        }

        void IInitializeSystem.Initialize()
        {
            Core.Context.CreateEntity().AddGameStage(EnumGameStage.Playing);
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.StackParameters);
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasStackParameters;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            var settings = Core.Helper.mainGameplaySettings;
            if (Core.Context.stackParameters.MaxRadius >= settings.minRadiusToContinueGame)
                return;
            
            Core.Context.ReplaceGameStage(EnumGameStage.WaitingForRestart);
        }
    }
}