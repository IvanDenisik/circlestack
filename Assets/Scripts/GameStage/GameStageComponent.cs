using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace GameStage
{
    [Game, Unique]
    public class GameStageComponent : IComponent
    {
        public EnumGameStage Stage;
    }
}