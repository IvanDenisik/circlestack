namespace GameStage
{
    public enum EnumGameStage
    {
        None = 0,
        WaitingForStartPlaying = 1,
        Playing = 2,
        WaitingForRestart = 3,
    }
}