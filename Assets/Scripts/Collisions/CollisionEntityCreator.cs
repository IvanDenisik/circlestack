using Common;
using UnityEngine;

namespace Collisions
{
    public class CollisionEntityCreator : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other) =>
            Core.Context.CreateEntity().AddTriggerEnter(gameObject, other.gameObject);

        private void OnTriggerExit(Collider other) =>
            Core.Context.CreateEntity().AddTriggerExit(gameObject, other.gameObject);

        private void OnCollisionEnter(Collision other) =>
            Core.Context.CreateEntity().AddCollisionEnter(gameObject, other.gameObject);

        private void OnCollisionExit(Collision other) =>
            Core.Context.CreateEntity().AddCollisionExit(gameObject, other.gameObject);
    }
}