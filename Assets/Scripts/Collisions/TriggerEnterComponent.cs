using Entitas;
using Entitas.CodeGeneration.Attributes;
using UnityEngine;

namespace Collisions
{
    [Game, Cleanup(CleanupMode.DestroyEntity)]
    public class TriggerEnterComponent : IComponent
    {
        public GameObject Source;
        public GameObject Target;
    }
}