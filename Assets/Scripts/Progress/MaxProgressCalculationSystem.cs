using Entitas;
using System.Collections.Generic;
using Common;

namespace Progress
{
    public class MaxProgressCalculationSystem : ReactiveSystem<GameEntity>, IInitializeSystem
    {
        public MaxProgressCalculationSystem() : base(Contexts.sharedInstance.game)
        {

        }

        void IInitializeSystem.Initialize()
        {
            if (!Core.Context.hasMaxProgress)
                Core.Context.CreateEntity().AddMaxProgress(0);
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.CurrentProgress);
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasCurrentProgress;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            if (Core.Context.maxProgress.Height >= Core.Context.currentProgress.Height)
                return;
            
            Core.Context.ReplaceMaxProgress(Core.Context.currentProgress.Height);
        }
    }
}