using DataSaving;
using Entitas;
using Entitas.CodeGeneration.Attributes;
using SceneManagement;

namespace Progress
{
    [Game, Unique, SaveLocaly, Global]
    public class MaxProgressComponent : IComponent
    {
        public int Height;
    }
}