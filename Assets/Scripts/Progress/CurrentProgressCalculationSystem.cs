using Entitas;
using System.Collections.Generic;
using Common;

namespace Progress
{
    public class CurrentProgressCalculationSystem : ReactiveSystem<GameEntity>, IInitializeSystem
    {
        public CurrentProgressCalculationSystem() : base(Contexts.sharedInstance.game)
        {

        }

        void IInitializeSystem.Initialize()
        {
            Core.Context.maxProgressEntity.AddCurrentProgress(0);
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.StackParameters);
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasStackParameters;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            var stackParams = Core.Context.stackParameters;
            if (stackParams.CirclesCount <= Core.Context.currentProgress.Height)
                return;
            
            Core.Context.ReplaceCurrentProgress(stackParams.CirclesCount);
        }
    }
}