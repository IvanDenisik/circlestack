namespace Progress
{
    public class ProgressSystems : Feature
    {
        public ProgressSystems() : base ("Progress Systems")
        {
            Add(new MaxProgressCalculationSystem());
            Add(new CurrentProgressCalculationSystem());
            Add(new ProgressViewSystem());
        }
    }
}