using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace Progress
{
    [Game, Unique]
    public class CurrentProgressComponent : IComponent
    {
        public int Height;
    }
}