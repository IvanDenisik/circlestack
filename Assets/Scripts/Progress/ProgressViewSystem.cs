using Entitas;
using System.Collections.Generic;
using Common;

namespace Progress
{
    public class ProgressViewSystem : ReactiveSystem<GameEntity>, IInitializeSystem
    {
        public ProgressViewSystem() : base(Contexts.sharedInstance.game)
        {

        }

        void IInitializeSystem.Initialize()
        {
            Core.Helper.currentProgressText.text = $"Circles: {Core.Context.currentProgress.Height}";
            Core.Helper.maxProgressText.text = $"Max: {Core.Context.maxProgress.Height}";
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.CurrentProgress);
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasCurrentProgress;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            Core.Helper.currentProgressText.text = $"Circles: {Core.Context.currentProgress.Height}";
        }
    }
}