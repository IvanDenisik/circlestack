using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace MainGameplay
{
    [Game, Unique, Cleanup(CleanupMode.DestroyEntity)]
    public class CircleAddedComponent : IComponent
    {
        public bool Perfect;
    }
}