using Entitas;

namespace MainGameplay
{
    [Game]
    public class HeightComponent : IComponent
    {
        public int Height;
    }
}