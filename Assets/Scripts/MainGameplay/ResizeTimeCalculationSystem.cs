using Entitas;
using System.Collections.Generic;
using Common;
using UnityEngine;

namespace MainGameplay
{
    /// <summary>
    /// Class calculates new resize time when player adds circle to the stack
    /// </summary>
    public class ResizeTimeCalculationSystem : ReactiveSystem<GameEntity>, IInitializeSystem
    {
        public ResizeTimeCalculationSystem() : base(Contexts.sharedInstance.game)
        {

        }
        
        void IInitializeSystem.Initialize()
        {
            Core.Context.stackParametersEntity.AddResizeTime
            (
                Core.Helper.mainGameplaySettings.maxResizeTime
            );
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.StackParameters);
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasStackParameters;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            var settings = Core.Helper.mainGameplaySettings;
            var stackParams = Core.Context.stackParameters;
            var newMaxRadius = stackParams.MaxRadius;
            var newResizeTime = settings.minResizeTime +
                                settings.resizeTimeFromMaxRadius.Evaluate(newMaxRadius / settings.maxCircleRadius) *
                                (settings.maxResizeTime - settings.minResizeTime);
            var resizeTimeDelta = settings.maxResizeTime * settings.resizeTimeRandomDeltaInPercentFromMax / 100f;
            newResizeTime = Core.RandomHelper.Random(newResizeTime - resizeTimeDelta, newResizeTime + resizeTimeDelta);
            newResizeTime = Mathf.Clamp(newResizeTime, settings.minResizeTime, settings.maxResizeTime);
            Core.Context.stackParametersEntity.ReplaceResizeTime
            (
                newResizeTime
            );
        }
    }
}