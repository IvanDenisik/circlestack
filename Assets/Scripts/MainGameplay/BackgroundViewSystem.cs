using Entitas;
using System.Collections.Generic;
using Common;
using DG.Tweening;
using UnityEngine;

namespace MainGameplay
{
    public class BackgroundViewSystem : ReactiveSystem<GameEntity>, IInitializeSystem
    {
        public BackgroundViewSystem() : base(Contexts.sharedInstance.game)
        {

        }

        void IInitializeSystem.Initialize()
        {
            ChangeBackgroundColor(0);
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.StackParameters);
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasStackParameters;
        }

        private void ChangeBackgroundColor(float changeTime)
        {
            var settings = Core.Helper.mainGameplaySettings;
            var stackParameters = Core.Context.stackParameters;
            var upColorPos = 1f * stackParameters.CirclesCount / settings.maxHeightForColorNotChanging;
            var downColorPos = upColorPos + settings.backgroundColorTimeDelta;
            upColorPos -= Mathf.Floor(upColorPos);
            downColorPos -= Mathf.Floor(downColorPos);
            var downColor = settings.backgroundColorOverHeight.Evaluate(upColorPos);
            var upColor = settings.backgroundColorOverHeight.Evaluate(downColorPos);
            Core.Helper.backgroundDown.DOColor(downColor, changeTime);
            Core.Helper.backgroundUp.DOColor(upColor, changeTime);
        }

        protected override void Execute(List<GameEntity> entities)
        {
            ChangeBackgroundColor(Core.Helper.mainGameplaySettings.backgroundColorChangeTime);
        }
    }
}