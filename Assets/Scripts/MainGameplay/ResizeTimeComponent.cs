using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace MainGameplay
{
    [Game, Unique]
    public class ResizeTimeComponent : IComponent
    {
        public float ResizeTime;
    }
}