using Entitas;
using System.Collections.Generic;
using System.Linq;
using Common;
using GameStage;
using UnityEngine;

namespace MainGameplay
{
    /// <summary>
    /// System creates circles entities, their gameobjects, components
    /// </summary>
    public class CircleEntityCreatorSystem : ReactiveSystem<GameEntity>
    {
        private static readonly int Color = Shader.PropertyToID("_Color");

        public CircleEntityCreatorSystem() : base(Contexts.sharedInstance.game)
        {

        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.StackParameters);
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasStackParameters;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            var activeCircle = Core.Context.GetGroup(GameMatcher.Circle).GetEntities().ToList()
                .Find(c => c.isActive);
            if (activeCircle != null)
                activeCircle.isActive = false;
            
            if (Core.Context.gameStage.Stage == EnumGameStage.WaitingForRestart)
                return;
            
            var e = Core.Context.CreateEntity();
            e.isCircle = true;
            var obj = Object.Instantiate(Core.Helper.circlePrefab, Core.Helper.circlesParent);
            var settings = Core.Helper.mainGameplaySettings;
            var stackParameters = Core.Context.stackParameters;
            obj.transform.position = new Vector3
            (
                0,
                stackParameters.CirclesCount * settings.circleHeight +
                settings.activeCircleOffsetFromStack,
                0
            );
            obj.transform.localScale = new Vector3
            (
                settings.minCicleRadius,
                obj.transform.localScale.y,
                settings.minCicleRadius
            );
            e.AddGameObject(obj);
            e.AddRadius(settings.minCicleRadius, 0, true, 0);
            e.isActive = true;
            e.AddHeight(stackParameters.CirclesCount);

            var colorPos = 1f * (stackParameters.CirclesCount + 1) / settings.maxHeightForColorNotChanging;
            colorPos -= Mathf.Floor(colorPos);
            var newColor = settings.circlesColorsOverHeight.Evaluate(colorPos);
            obj.GetComponent<Renderer>().material.color = newColor;
        }
    }
}