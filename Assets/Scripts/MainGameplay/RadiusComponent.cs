using Entitas;

namespace MainGameplay
{
    [Game]
    public class RadiusComponent : IComponent
    {
        public float Radius;
        public float Time;
        public bool Growing;
        public float ResizeTime;
    }
}