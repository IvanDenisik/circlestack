using Entitas;
using System.Collections.Generic;
using System.Linq;
using Common;
using DG.Tweening;
using UnityEngine;

namespace MainGameplay
{
    /// <summary>
    /// System puts circles on top of the stack.
    /// </summary>
    public class CirclesMoveSystem : ReactiveSystem<GameEntity>
    {
        public CirclesMoveSystem() : base(Contexts.sharedInstance.game)
        {

        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.StackParameters);
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasStackParameters;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            var activeCircle = Core.Context.GetGroup(GameMatcher.Circle).GetEntities().ToList()
                .Find(e => e.isActive);
            if (activeCircle == null)
                return;

            var settings = Core.Helper.mainGameplaySettings;
            activeCircle.gameObject.Obj.transform.DOMove(
                new Vector3
                (
                    0,
                    settings.circleHeight * Core.Context.stackParameters.CirclesCount,
                    0
                ),
                settings.circleFallTime
            );
        }
    }
}