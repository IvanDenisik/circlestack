using System.Linq;
using Common;
using Entitas;
using UnityEngine;

namespace MainGameplay
{
    /// <summary>
    /// Systems changes radius of active circle every frame using settings.
    /// </summary>
    public class CircleRadiusChangerSystem : IExecuteSystem
    {
        void IExecuteSystem.Execute()
        {
            if (!Core.Context.isTapping)
                return;
            
            var activeCircle = Core.Context.GetGroup(GameMatcher.Circle).GetEntities().ToList().Find(e => e.isActive);
            if (activeCircle == null)
                return;
            
            var settings = Core.Helper.mainGameplaySettings;
            var stackParameters = Core.Context.stackParameters;
            var resizeTime = Core.Context.resizeTime;
            var newTime = activeCircle.radius.Time;
            var newGrowing = activeCircle.radius.Growing;
            if (activeCircle.radius.Growing)
            {
                newTime += Time.deltaTime;
                if (newTime >= resizeTime.ResizeTime)
                {
                    newTime = resizeTime.ResizeTime;
                    newGrowing = false;
                }
            }
            else
            {
                newTime -= Time.deltaTime;
                if (newTime <= 0)
                {
                    newTime = 0;
                    newGrowing = true;
                }
            }

            var newRadius = settings.minCicleRadius +
                            settings.radiusFromTime.Evaluate(newTime / resizeTime.ResizeTime) *
                            (stackParameters.MaxRadius - settings.minCicleRadius);
            activeCircle.ReplaceRadius(newRadius, newTime, newGrowing, 0);
        }
    }
}