using Entitas;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace MainGameplay
{
    /// <summary>
    /// System deals with visual effects of circles.
    /// </summary>
    public class CirclesViewSystem : ReactiveSystem<GameEntity>
    {
        public CirclesViewSystem() : base(Contexts.sharedInstance.game)
        {

        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.Radius);
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.isCircle && entity.hasRadius;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var e in entities)
            {
                var circleTransform = e.gameObject.Obj.transform;
                circleTransform.DOKill();
                circleTransform.DOScale(
                    new Vector3
                    (
                        e.radius.Radius,
                        circleTransform.localScale.y,
                        e.radius.Radius
                    ),
                    e.radius.ResizeTime);
            }
        }
    }
}