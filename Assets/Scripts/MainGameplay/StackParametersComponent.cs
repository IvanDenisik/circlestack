using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace MainGameplay
{
    [Game, Unique]
    public class StackParametersComponent : IComponent
    {
        public int CirclesCount;
        public float MaxRadius;
        public float WorldStackHeight;
    }
}