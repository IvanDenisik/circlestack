using System;
using UnityEngine;

namespace MainGameplay
{
    [Serializable, CreateAssetMenu(fileName = "New MainGameplaySettings", menuName = "Project Settings/MainGameplaySettings")]
    public class MainGameplaySettings : ScriptableObject
    {
        [Header("Base")]
        public float circleHeight;
        public float activeCircleOffsetFromStack;
        public float circleFallTime;

        [Header("Radius changing")]
        public float minCicleRadius;
        public float maxCircleRadius;
        public AnimationCurve radiusFromTime;
        public float minRadiusToContinueGame;
        
        public float minResizeTime;
        public float maxResizeTime;
        public AnimationCurve resizeTimeFromMaxRadius;
        public float resizeTimeRandomDeltaInPercentFromMax;

        public float perfectError;
        public int circlesCountToEnlargeFromPerfect;
        public float perfectRadiusDelta;
        public float perfectResizeTime;
        public int perfectInARowForEffect;
        public float perfectPSOffsetY;
        public Color perfectPSColor;
        public Color perfectPSColorInARow;
        public float perfectPSInRowSizeKoeff;

        [Header("Colors")]
        public int maxHeightForColorNotChanging;
        public Gradient circlesColorsOverHeight;
        public Gradient backgroundColorOverHeight;
        public float backgroundColorTimeDelta;
        public float backgroundColorChangeTime;
    }
}