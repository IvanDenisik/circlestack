using Entitas;
using System.Collections.Generic;
using System.Linq;
using Common;
using GameStage;
using UnityEngine;

namespace MainGameplay
{
    /// <summary>
    /// System deals with logic when you add circle to stack: stack grows, max radius changed, circle can be perfect and so on.
    /// </summary>
    public class AddCircleToStackSystem : ReactiveSystem<GameEntity>, IInitializeSystem
    {
        public AddCircleToStackSystem() : base(Contexts.sharedInstance.game) {}

        void IInitializeSystem.Initialize()
        {
            Core.Context.CreateEntity().AddStackParameters
            (
                0,
                Core.Helper.mainGameplaySettings.maxCircleRadius,
                0
            );
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.TapUp);
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.isTapUp;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            if (Core.Context.gameStage.Stage == EnumGameStage.WaitingForRestart)
                return;
            
            var settings = Core.Helper.mainGameplaySettings;
            var stackParams = Core.Context.stackParameters;
            var activeCircle = Core.Context.GetGroup(GameMatcher.Circle).GetEntities().ToList()
                .Find(e => e.isActive);
            var newMaxRadius = activeCircle.radius.Radius;
            var isPerfect = false;
            if (Mathf.Abs(stackParams.MaxRadius - newMaxRadius) <= settings.perfectError)
            {
                newMaxRadius = stackParams.MaxRadius;
                activeCircle.ReplaceRadius(newMaxRadius, activeCircle.radius.Time, activeCircle.radius.Growing, 0);
                isPerfect = true;
                activeCircle.isPerfect = isPerfect;
            }

            Core.Context.ReplaceStackParameters
            (
                stackParams.CirclesCount + 1,
                newMaxRadius,
                (stackParams.CirclesCount + 1) * settings.circleHeight
            );

            Core.Context.CreateEntity().AddCircleAdded(isPerfect);
        }
    }
}