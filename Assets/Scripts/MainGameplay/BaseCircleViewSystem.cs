using Common;
using Entitas;
using UnityEngine;

namespace MainGameplay
{
    public class BaseCircleViewSystem : IInitializeSystem
    {
        void IInitializeSystem.Initialize()
        {
            var settings = Core.Helper.mainGameplaySettings;
            var newColor = settings.circlesColorsOverHeight.Evaluate(0);
            var circle = Core.Helper.baseCircle;
            circle.GetComponent<Renderer>().material.color = newColor;
            circle.transform.localScale = new Vector3
            (
                settings.maxCircleRadius,
                circle.transform.localScale.y,
                settings.maxCircleRadius
            );
        }
    }
}