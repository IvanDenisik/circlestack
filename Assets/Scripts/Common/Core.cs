using HierarchyHelpers;
using MainGameplay;

namespace Common
{
    /// <summary>
    /// Class simplifies access to main game entities.
    /// </summary>
    public static class Core
    {
        public static GameContext Context => Contexts.sharedInstance.game;
        public static RandomHelper.RandomHelper RandomHelper => new RandomHelper.RandomHelper();
        public static GameHierarchyHelper Helper => Context.gameHierarchyHelper.instance;
    }
}