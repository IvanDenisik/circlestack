using Entitas;
using UnityEngine;

namespace Common
{
    [Game]
    public class GameObjectComponent : IComponent
    {
        public GameObject Obj;
    }
}