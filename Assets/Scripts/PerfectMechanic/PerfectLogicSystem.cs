using System.Collections.Generic;
using System.Linq;
using Common;
using Entitas;
using UnityEngine;

namespace PerfectMechanic
{
    public class PerfectLogicSystem : ReactiveSystem<GameEntity>
    {
        public PerfectLogicSystem() : base(Contexts.sharedInstance.game)
        {

        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.PerfectParameters);
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasPerfectParameters;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            var settings = Core.Helper.mainGameplaySettings;
            if (Core.Context.perfectParameters.PerfectsInARow < settings.perfectInARowForEffect)
                return;

            var topCircles = Core.Context.GetGroup(GameMatcher.Circle).GetEntities().ToList()
                .OrderByDescending(e => e.height.Height).Take(settings.circlesCountToEnlargeFromPerfect).ToList();
            var stackParameters = Core.Context.stackParameters;

            var newMaxRadius = Mathf.Min
            (
                settings.maxCircleRadius,
                stackParameters.MaxRadius + settings.perfectRadiusDelta
            );
            Core.Context.ReplaceStackParameters
            (
                stackParameters.CirclesCount,
                newMaxRadius,
                stackParameters.WorldStackHeight
            );
            
            foreach (var circle in topCircles)
            {
                if (circle.radius.Radius >= newMaxRadius)
                    continue;

                var newRadius = Mathf.Min
                (
                    newMaxRadius,
                    circle.radius.Radius + settings.perfectRadiusDelta
                );
                circle.ReplaceRadius(newRadius, circle.radius.Time, circle.radius.Growing, settings.perfectResizeTime);
            }
        }
    }
}