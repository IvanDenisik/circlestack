using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace PerfectMechanic
{
    [Game, Unique]
    public class PerfectParametersComponent : IComponent
    {
        public int PerfectsInARow;
    }
}