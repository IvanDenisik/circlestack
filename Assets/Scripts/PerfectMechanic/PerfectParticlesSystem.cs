using Entitas;
using System.Collections.Generic;
using Common;
using UnityEngine;

namespace PerfectMechanic
{
    public class PerfectParticlesSystem : ReactiveSystem<GameEntity>
    {
        public PerfectParticlesSystem() : base(Contexts.sharedInstance.game)
        {

        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.CircleAdded);
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasCircleAdded && entity.circleAdded.Perfect;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            var settings = Core.Helper.mainGameplaySettings;
            var ps = Core.Helper.perfectPS;
            ps.transform.position =
                Vector3.up * (Core.Context.stackParameters.WorldStackHeight + settings.perfectPSOffsetY);
            var psMain = ps.main;
            var perfectsInARow = Core.Context.perfectParameters.PerfectsInARow >= settings.perfectInARowForEffect;
            var sizeKoef = perfectsInARow ? settings.perfectPSInRowSizeKoeff : 1;
            psMain.startSize = Core.Context.stackParameters.MaxRadius * sizeKoef;
            psMain.startColor = perfectsInARow
                ? settings.perfectPSColorInARow
                : settings.perfectPSColor;
            ps.Emit(1);
        }
    }
}