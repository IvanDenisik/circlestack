using Entitas;
using System.Collections.Generic;
using System.Linq;
using Common;

namespace PerfectMechanic
{
    public class PerfectInARowCalculatorSystem : ReactiveSystem<GameEntity>, IInitializeSystem
    {
        public PerfectInARowCalculatorSystem() : base(Contexts.sharedInstance.game)
        {

        }

        void IInitializeSystem.Initialize()
        {
            Core.Context.stackParametersEntity.AddPerfectParameters(0);
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.CircleAdded);
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasCircleAdded;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            var newPerfectsInARow = 0;
            if (Core.Context.circleAdded.Perfect)
                newPerfectsInARow = Core.Context.perfectParameters.PerfectsInARow + 1;
            
            Core.Context.ReplacePerfectParameters(newPerfectsInARow);
        }
    }
}