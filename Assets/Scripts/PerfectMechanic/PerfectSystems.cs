namespace PerfectMechanic
{
    public class PerfectSystems : Feature
    {
        public PerfectSystems() : base ("Perfect Systems")
        {
            Add(new PerfectInARowCalculatorSystem());
            Add(new PerfectLogicSystem());
            Add(new PerfectVibrationSystem());
            Add(new PerfectParticlesSystem());
        }
    }
}