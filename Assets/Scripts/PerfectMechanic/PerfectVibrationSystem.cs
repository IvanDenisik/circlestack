using System.Collections.Generic;
using Entitas;
using MoreMountains.NiceVibrations;
using UnityEngine;

namespace PerfectMechanic
{
    public class PerfectVibrationSystem : ReactiveSystem<GameEntity>
    {
        public PerfectVibrationSystem() : base(Contexts.sharedInstance.game)
        {

        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.Perfect);
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.isCircle && entity.isPerfect;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            MMVibrationManager.Haptic (HapticTypes.MediumImpact);
        }
        
        static void KyVibrator()
        {
            // Trick Unity into giving the App vibration permission when it builds.
            // This check will always be false, but the compiler doesn't know that.
            if (Application.isEditor) Handheld.Vibrate();
        }
    }
}