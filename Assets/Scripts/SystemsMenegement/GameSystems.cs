using CameraMechanic;
using GameStage;
using HierarchyHelpers;
using MainGameplay;
using PerfectMechanic;
using Progress;
using SceneManagement;
using ScreenTapMechanic;

namespace SystemsMenegement
{
    public class GameSystems : Feature
    {
        public GameSystems() : base ("Game Scene Systems")
        {
            Add(new GameHierarchyHelperCreatorSystem());
            Add(new DebugButtonSystem());

            Add(new TapDetectionSystem());

            Add(new AddCircleToStackSystem());
            Add(new FinishGameSystem());
            Add(new ResizeTimeCalculationSystem());
            
            Add(new PerfectSystems());
            
            Add(new CirclesMoveSystem());
            Add(new CircleEntityCreatorSystem());
            Add(new CircleRadiusChangerSystem());
            Add(new CirclesViewSystem());
            Add(new BaseCircleViewSystem());

            Add(new BackgroundViewSystem());

            Add(new CameraSystems());
            Add(new ProgressSystems());
            
            Add(new DestroyEntitiesSystem());
        }
    }
}