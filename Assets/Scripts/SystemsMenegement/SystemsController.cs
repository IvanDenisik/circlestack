using System.Collections.Generic;
using Entitas;
using SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SystemsMenegement
{
    public class SystemsController: MonoBehaviour
    {
        private Systems globalSystems;
        private Systems gameSystems;
        
        private List<string> activeScenes = new List<string>();

        private void Start()
        {
            globalSystems = new GlobalSystems();
            gameSystems = new GameSystems();
            
            globalSystems.Initialize();
        }

        private void OnEnable()
        {
            SceneManager.sceneLoaded += SceneLoaded;
            SceneManager.sceneUnloaded += SceneUnloaded;
        }
        
        private void OnDisable()
        {
            SceneManager.sceneLoaded -= SceneLoaded;
            SceneManager.sceneUnloaded -= SceneUnloaded;
        }

        private void SceneLoaded(Scene scene, LoadSceneMode loadMode)
        {
            activeScenes.Add(scene.name);
            
            if (scene.name == SceneNames.Game)
                gameSystems.Initialize();
        }
        
        private void Update()
        {
            if (activeScenes.Contains(SceneNames.Game))
                gameSystems.Execute();
            globalSystems.Execute();
            
            if (activeScenes.Contains(SceneNames.Game))
                gameSystems.Cleanup();
            globalSystems.Cleanup();
        }
        
        private void SceneUnloaded(Scene scene)
        {
            activeScenes.Remove(scene.name);
            
            if (scene.name == SceneNames.Game)
                gameSystems.TearDown();
            
            if (scene.name == SceneNames.Global)
                globalSystems.TearDown();
        }
    }
}