using DataSaving;
using FPS;
using HierarchyHelpers;
using RandomHelper;
using SceneManagement;

namespace SystemsMenegement
{
    public class GlobalSystems : Feature
    {
        public GlobalSystems() : base ("Global Systems")
        {
            Add(new SetFPSSystem());
            
            Add(new GlobalHierarchyHelperCreatorSystem());
            Add(new LoadDataSystem());
            Add(new LoadGameSceneSystem());
            Add(new SaveDataSystem());
            Add(new RestartSceneSystem());
            Add(new GameCleanupSystems(Contexts.sharedInstance));
        }
    }
}