using Entitas;
using UnityEngine;

namespace FPS
{
    public class SetFPSSystem : IInitializeSystem
    {
        void IInitializeSystem.Initialize()
        {
            Application.targetFrameRate = 60;
        }
    }
}