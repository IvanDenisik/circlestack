using Common;
using Entitas;
using UnityEngine;

namespace ScreenTapMechanic
{
    public class TapDetectionSystem : IInitializeSystem, IExecuteSystem
    {
        private bool taped;
        private bool tapedUp;
        
        void IInitializeSystem.Initialize()
        {
            Core.Helper.screenTapCatcher.OnDown += TapCatcherDown;
            Core.Helper.screenTapCatcher.OnUp += TapCatcherUp;
        }

        private void TapCatcherDown()
        {
            taped = true;
        }
        
        private void TapCatcherUp()
        {
            tapedUp = true;
        }

        void IExecuteSystem.Execute()
        {
            if (taped)
            {
                taped = false;
                Core.Context.isTap = true;
                Core.Context.isTapping = true;
            }
            
            if (tapedUp)
            {
                tapedUp = false;
                Core.Context.isTapUp = true;
                Core.Context.isTapping = false;
            }
        }
    }
}