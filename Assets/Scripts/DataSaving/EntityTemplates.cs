using System;
using System.Collections.Generic;

namespace DataSaving
{
    [Serializable]
    public class EntityTemplates
    {
        public List<EntityTemplate> list = new List<EntityTemplate>();
    }
}