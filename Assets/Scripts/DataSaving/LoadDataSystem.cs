using System;
using System.Collections.Generic;
using System.Linq;
using Entitas;
using UnityEngine;

namespace DataSaving
{
    public class LoadDataSystem : IInitializeSystem
    {
        void IInitializeSystem.Initialize()
        {
            var context = Contexts.sharedInstance.game;
            if (!context.globalHierarchyHelper.instance.saveLoadSettings.loadDataAtGameStart)
                return;

            var serverData = new EntityTemplates();
            //TODO: реализовать серверную часть при необходимости

            var localDataJson = PlayerPrefs.GetString("LocalData");
            var localData = JsonUtility.FromJson<EntityTemplates>(localDataJson);
            if (localData == null)
                return;
            
            foreach (var template in localData.list)
            {
                var e = context.CreateEntity();
                AddFlagComponents(template, e);
                AddNonFlagComponents(template, e);
            }
        }

        private void AddFlagComponents(EntityTemplate template, IEntity newEntity)
        {
            foreach (var name in template.flagComponentsNames)
            {
                if (!GameComponentsLookup.componentNames.ToList().Contains(name))
                    Debug.LogError($"{name} is not in GameComponentsLookup");
                
                var componentLookUpIndex =
                    (int) typeof(GameComponentsLookup).GetField(name).GetValue(null);
                var componentType = GameComponentsLookup.componentTypes[componentLookUpIndex];
                var tagComponent = Activator.CreateInstance(componentType);

                ((Entity) newEntity).AddComponent(componentLookUpIndex, tagComponent as IComponent);
            }
        }
        
        private void AddNonFlagComponents(EntityTemplate template, IEntity newEntity)
        {
            for (var i = 0; i < template.nonFlagComponentsNames.Length; i++)
            {
                var name = template.nonFlagComponentsNames[i];
                if (!GameComponentsLookup.componentNames.ToList().Contains(name))
                    Debug.LogError($"{name} is not in GameComponentsLookup");
                
                var componentLookUpIndex = (int)typeof(GameComponentsLookup).GetField(name).GetValue(null);
                var componentType = GameComponentsLookup.componentTypes[componentLookUpIndex];
                var component = JsonUtility.FromJson(template.nonFlagComponents[i], componentType);

                ((Entity)newEntity).AddComponent(componentLookUpIndex, component as IComponent);
            }
        }
    }
}