using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace DataSaving
{
    [Game, Unique, Cleanup(CleanupMode.DestroyEntity)]
    public class SaveDataComponent : IComponent
    {
        public EnumDataType[] DataTypes;
    }
}