using System;
using UnityEngine;

namespace DataSaving
{
    [Serializable, CreateAssetMenu(fileName = "New SaveLoadSettings", menuName = "Project Settings/SaveLoadSettings")]
    public class SaveLoadSettings : ScriptableObject
    {
        public bool loadDataAtGameStart;
    }
}