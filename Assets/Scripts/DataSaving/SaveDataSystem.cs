using System;
using System.Collections.Generic;
using Entitas;
using UnityEngine;

namespace DataSaving
{
    public class SaveDataSystem : IExecuteSystem
    {
        void IExecuteSystem.Execute()
        {
            var context = Contexts.sharedInstance.game;
            if (!context.hasSaveData)
                return;

            var localData = new EntityTemplates();
            var serverData = new EntityTemplates();
            
            foreach (var entity in context.GetEntities())
            {
                foreach (var saveType in context.saveData.DataTypes)
                {
                    var template = MakeEntityTemplate(entity, saveType);
                    if (template == null)
                        break;

                    switch (saveType)
                    {
                        case EnumDataType.Local:
                            localData.list.Add(template);
                            break;
                        case EnumDataType.Server:
                            serverData.list.Add(template);
                            break;
                    }
                }
            }
            
            //Сохраняем локально
            var localDataJson = JsonUtility.ToJson(localData);
            PlayerPrefs.SetString("LocalData", localDataJson);
            
            //Сохраняем на сервере
            //TODO: реализовать серверную часть при необходимости
        }
        
        private string EntityToJson(IEntity entity, EnumDataType dataType)
            => JsonUtility.ToJson(MakeEntityTemplate(entity, dataType));

        private EntityTemplate MakeEntityTemplate(IEntity entity, EnumDataType dataType)
        {
            if (entity.GetComponents().Length == 0 || dataType == EnumDataType.None)
                return null;
            
            var flagComponentsNames = new List<string>();
            var componentsNames = new List<string>();
            var components = new List<string>();
                
            foreach (var component in entity.GetComponents())
            {
                if (dataType == EnumDataType.Local && !HasSaveLocalyAttribute(component))
                    continue;
                
                if (dataType == EnumDataType.Server && !HasSaveOnServerAttribute(component))
                    continue;

                var componentName = GetComponentName(component);

                if (IsFlagComponent(component))
                {
                    flagComponentsNames.Add(componentName);
                }
                else
                {
                    componentsNames.Add(componentName);
                    components.Add(JsonUtility.ToJson(component));
                }
            }

            if (flagComponentsNames.Count == 0 && componentsNames.Count == 0)
                return null;

            var template = new EntityTemplate
            {
                entityId = entity.creationIndex,
                flagComponentsNames = flagComponentsNames.ToArray(),
                nonFlagComponentsNames = componentsNames.ToArray(),
                nonFlagComponents = components.ToArray(),
            };

            return template;
        }

        private bool HasSaveLocalyAttribute(object obj)
        {
            var t = obj.GetType();
            return Attribute.IsDefined(t, typeof(SaveLocalyAttribute));
        }
        
        private bool HasSaveOnServerAttribute(object obj)
        {
            var t = obj.GetType();
            return Attribute.IsDefined(t, typeof(SaveLocalyAttribute));
        }

        private string GetComponentName(IComponent component)
        {
            var fullName = component.GetType().Name;
            return fullName.EndsWith("Component") 
                ? fullName.Remove(fullName.Length - 9, 9)
                : fullName;
        }

        private bool IsFlagComponent(IComponent component)
        {
            return component.GetType().GetFields().Length == 0;
        }
    }
}