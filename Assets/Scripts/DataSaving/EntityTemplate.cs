using System;

namespace DataSaving
{
    [Serializable]
    public class EntityTemplate
    {
        public int entityId;
        public string[] flagComponentsNames;
        public string[] nonFlagComponentsNames;
        public string[] nonFlagComponents;
    }
}