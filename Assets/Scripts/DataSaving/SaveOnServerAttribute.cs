using System;

namespace DataSaving
{
    [AttributeUsage(AttributeTargets.Class)]
    public class SaveOnServerAttribute : System.Attribute
    {
    }
}