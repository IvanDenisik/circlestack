﻿using System.Linq;
using UnityEngine;

namespace RandomHelper
{
    /// <summary> Класс с полезными (не только в этой игре) часто используемыми методами, свойствами </summary>
    public class RandomHelper
    {
        /// <summary> Метод возвращает случайный элемент с равномерным распределением </summary>
        public T RandomElement<T>(params T[] elements)
        {
            return elements[UnityEngine.Random.Range(0, elements.Length)];
        }

        public T Random<T>(T[] array, float[] weights)
        {
            if (array.Length == 0)
            {
                return default(T);
            }
            else if (array.Length != weights.Length)
            {
                Debug.LogErrorFormat("Utils:random(list, weights):list.count != weights.count!!! list.count = {0}, weights.count = {1}", array.Length, weights.Length);
                return default(T);
            }
            else
            {
                var sum = weights.Sum();
                float curr = 0;
                var random = UnityEngine.Random.Range(0, sum);
                for (int i = 0; i < array.Length; i++)
                {
                    curr += weights[i];
                    if (random <= curr)
                        return array[i];
                }

                return default(T);
            }
        }

        /// <summary> Метод возвращает случайное число от 0 (включительно) до count (исключительно) </summary>
        public int Random(int count)
        {
            return UnityEngine.Random.Range(0, count);
        }

        /// <summary> Метод возвращает случайное число от min (включительно) до max (исключительно) </summary>
        public int Random(int min, int max)
        {
            return UnityEngine.Random.Range(min, max);
        }
    
        /// <summary> Метод возвращает случайное число от min до max</summary>
        public float Random(float min, float max)
        {
            return UnityEngine.Random.Range(min, max);
        }

        /// <summary> Метод возвращает true, если сгенерированное число меньше chance, иначе false. chance -> [0, 1] </summary>
        public bool Random(float chance)
        {
            return UnityEngine.Random.Range(0f, 1f) <= chance;
        }
    
        /// <summary> Метод возвращает случайное число от 0 (включительно) до weights.Lenght (исключительно) с заданными весами  </summary>
        public int Random(params float[] weights)
        {
            if (weights.Length == 0)
                return -1;

            var sum = weights.Sum();
            float curr = 0;
            var random = UnityEngine.Random.Range(0, sum);
            for (int i = 0; i < weights.Length; i++)
            {
                curr += weights[i];
                if (random <= curr)
                    return i;
            }

            return -1;
        }
    
        /// <summary>
        /// Метод возвращает случайную точку внутри заданного RectTransform на заданном расстроянии от lastCenter и centers
        /// </summary>
        public Vector2 RandomPointFromZone(RectTransform rect, Vector2 lastCenter = default, Vector2[] centers = default, float minRadius = 0, float maxRadius = 100500, bool local = true)
        {
            var corners = new Vector3[4];
            if (local)
                rect.GetLocalCorners(corners);
            else 
                rect.GetWorldCorners(corners);

            if (minRadius > corners[3].x - corners[0].x && minRadius > corners[1].y - corners[0].y)
            {
                Debug.LogError("Wrong zone settings!");
                return Vector2.zero;
            }

            if (minRadius == 0 && maxRadius == 100500)
                return new Vector2(Random(corners[0].x, corners[3].x), Random(corners[0].y, corners[1].y)) +
                       (Vector2) rect.localPosition;
            
            for (var i = 0; i < 100; i++)
            {
                var point = new Vector2(Random(corners[0].x, corners[3].x), Random(corners[0].y, corners[1].y));
                point += (Vector2) rect.localPosition;
                float minDistance = 0;
                if (centers.Length > 0)
                    minDistance = centers.Select(c => (c - point).magnitude).Min();
                var distance = (lastCenter - point).magnitude;
                if (minRadius <= minDistance && distance <= maxRadius)
                    return point;
            }

            return rect.localPosition;
        }
    }
}
