using Entitas;

namespace Templates
{
    public class CleanupTemplateSystem : ICleanupSystem
    {
        void ICleanupSystem.Cleanup()
        {
            
        }
    }
}

/*
Command: cleanup_system

using Entitas;

namespace $NAMESPACE$
{
    public class $CLASS$ : ICleanupSystem
    {
        void ICleanupSystem.Cleanup()
        {
            $END$
        }
    }
}
*/