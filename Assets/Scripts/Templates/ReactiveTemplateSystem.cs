using Entitas;
using System.Collections.Generic;

namespace Templates
{
    public class ReactiveTemplateSystem : ReactiveSystem<GameEntity>
    {
        public ReactiveTemplateSystem() : base(Contexts.sharedInstance.game)
        {

        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.GlobalHierarchyHelper);
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasGlobalHierarchyHelper;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var e in entities)
            {
                
            }
        }
    }
}

/*
Command: reactive_system

using Entitas;
using System.Collections.Generic;

namespace $NAMESPACE$
{
    public class $CLASS$ : ReactiveSystem<GameEntity>
    {
        public $CLASS$() : base(Contexts.sharedInstance.game)
        {

        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.GlobalHierarchyHelper);
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasGlobalHierarchyHelper;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var e in entities)
            {
                $END$
            }
        }
    }
}
*/