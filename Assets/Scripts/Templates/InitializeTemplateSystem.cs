using Entitas;

namespace Templates
{
    public class InitializeTemplateSystem : IInitializeSystem
    {
        void IInitializeSystem.Initialize()
        {
            
        }
    }
}

/*
Command: initialize_system

using Entitas;

namespace $NAMESPACE$
{
    public class $CLASS$ : IInitializeSystem
    {
        void IInitializeSystem.Initialize()
        {
            $END$
        }
    }
}
*/