using Entitas;

namespace Templates
{
    public class TearDownTemplateSystem : ITearDownSystem
    {
        void ITearDownSystem.TearDown()
        {
            
        }
    }
}


/*
Command: teardown_system

using Entitas;

namespace $NAMESPACE$
{
    public class $CLASS$ : ITearDownSystem
    {
        void ITearDownSystem.TearDown()
        {
            $END$
        }
    }
}
*/