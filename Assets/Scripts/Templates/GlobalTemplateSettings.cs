using System;
using UnityEngine;

namespace Templates
{
    [Serializable, CreateAssetMenu(fileName = "New GlobalTemplateSettings", menuName = "Project Settings/GlobalTemplateSettings")]
    public class GlobalTemplateSettings : ScriptableObject
    {
        public float speed;
    }
}