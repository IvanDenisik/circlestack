using Entitas;

namespace Templates
{
    public class ExecuteTemplateSystem : IExecuteSystem
    {
        void IExecuteSystem.Execute()
        {
            
        }
    }
}

/*
Command: execute_system

using Entitas;

namespace $NAMESPACE$
{
    public class $CLASS$ : IExecuteSystem
    {
        void IExecuteSystem.Execute()
        {
            $END$
        }
    }
}

*/