using System;
using TMPro;
using UnityEngine;

namespace Templates
{
    [Serializable]
    public class TemplateView
    {
        public TextMeshProUGUI text;
    }
}