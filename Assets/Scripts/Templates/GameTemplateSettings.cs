using System;
using UnityEngine;

namespace Templates
{
    [Serializable, CreateAssetMenu(fileName = "New GameTemplateSettings", menuName = "Project Settings/GameTemplateSettings")]
    public class GameTemplateSettings : ScriptableObject
    {
        public float power;
    }
}