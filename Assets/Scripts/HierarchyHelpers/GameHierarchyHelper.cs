using System;
using CameraMechanic;
using Common;
using MainGameplay;
using Templates;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace HierarchyHelpers
{
    [Serializable]
    public class GameHierarchyHelper : MonoBehaviour
    {
        [Header("Debug")]
        public Button debugButton;

        [Header("Input")] 
        public SpriteTapCatcher screenTapCatcher;

        [Header("Settings")] 
        public MainGameplaySettings mainGameplaySettings;
        public CameraSettings cameraSettings;

        [Header("View")] 
        public GameObject circlePrefab;
        public Transform circlesParent;
        public TextMeshProUGUI maxProgressText;
        public TextMeshProUGUI currentProgressText;
        public GameObject baseCircle;
        public Image backgroundDown;
        public Image backgroundUp;
        public ParticleSystem perfectPS;
    }
}