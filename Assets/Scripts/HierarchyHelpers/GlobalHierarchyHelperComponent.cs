using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace HierarchyHelpers
{
    [Game, Unique]
    public class GlobalHierarchyHelperComponent : IComponent
    {
        public GlobalHierarchyHelper instance;
    }
}