using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace HierarchyHelpers
{
    [Game, Unique]
    public class GameHierarchyHelperComponent : IComponent
    {
        public GameHierarchyHelper instance;
    }
}