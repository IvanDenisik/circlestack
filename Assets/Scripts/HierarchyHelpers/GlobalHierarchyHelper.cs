using DataSaving;
using Templates;
using UnityEngine;

namespace HierarchyHelpers
{
    public class GlobalHierarchyHelper : MonoBehaviour
    {
        public SaveLoadSettings saveLoadSettings;
        
        public GlobalTemplateSettings globalTemplateSettings;
    }
}