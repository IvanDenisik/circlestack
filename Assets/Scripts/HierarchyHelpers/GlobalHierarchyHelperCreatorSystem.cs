using Common;
using Entitas;
using UnityEngine;

namespace HierarchyHelpers
{
    public class GlobalHierarchyHelperCreatorSystem : IInitializeSystem
    {
        void IInitializeSystem.Initialize()
        {
            var e = Contexts.sharedInstance.game.CreateEntity();
            var helper = GameObject.FindGameObjectWithTag(Tags.GlobalHierarchyHelper)
                .GetComponent<GlobalHierarchyHelper>();
            e.AddGlobalHierarchyHelper(helper);
        }
    }
}