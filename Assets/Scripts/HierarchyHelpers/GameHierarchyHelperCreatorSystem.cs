using Common;
using Entitas;
using UnityEngine;

namespace HierarchyHelpers
{
    public class GameHierarchyHelperCreatorSystem : IInitializeSystem
    {
        void IInitializeSystem.Initialize()
        {
            var e = Contexts.sharedInstance.game.CreateEntity();
            var helper = GameObject.FindGameObjectWithTag(Tags.GameHierarchyHelper)
                .GetComponent<GameHierarchyHelper>();
            e.AddGameHierarchyHelper(helper);
        }
    }
}