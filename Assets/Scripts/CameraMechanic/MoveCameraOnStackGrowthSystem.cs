using System.Collections.Generic;
using Common;
using DG.Tweening;
using Entitas;

namespace CameraMechanic
{
    public class MoveCameraOnStackGrowthSystem : ReactiveSystem<GameEntity>, IInitializeSystem
    {
        public MoveCameraOnStackGrowthSystem() : base(Contexts.sharedInstance.game)
        {

        }

        void IInitializeSystem.Initialize()
        {
            Core.Context.CreateEntity().AddCameraStartOffset(UnityEngine.Camera.main.transform.position);
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.StackParameters);
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasStackParameters;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            UnityEngine.Camera.main.transform.DOMoveY(
                Core.Context.cameraStartOffset.Offset.y + Core.Context.stackParameters.WorldStackHeight, 
                Core.Helper.cameraSettings.cameraMoveTime);
        }
    }
}