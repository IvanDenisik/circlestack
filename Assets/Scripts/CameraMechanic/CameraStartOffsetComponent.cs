using Entitas;
using Entitas.CodeGeneration.Attributes;
using UnityEngine;

namespace CameraMechanic
{
    [Game, Unique]
    public class CameraStartOffsetComponent : IComponent
    {
        public Vector3 Offset;
    }
}