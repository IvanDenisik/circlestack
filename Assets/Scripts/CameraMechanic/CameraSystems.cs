namespace CameraMechanic
{
    public class CameraSystems : Feature
    {
        public CameraSystems() : base ("Camera Systems")
        {
            Add(new MoveCameraOnStackGrowthSystem());
            Add(new MoveCameraOnGameFinishedSystem());
        }
    }
}