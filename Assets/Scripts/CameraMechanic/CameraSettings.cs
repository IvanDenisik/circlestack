using System;
using UnityEngine;

namespace CameraMechanic
{
    [Serializable, CreateAssetMenu(fileName = "New CameraSettings", menuName = "Project Settings/CameraSettings")]
    public class CameraSettings : ScriptableObject
    {
        public float cameraMoveTime;
        public float moveCameraAwayDeltaPerStackHeight;
        public float moveCameraAwayTime;
    }
}