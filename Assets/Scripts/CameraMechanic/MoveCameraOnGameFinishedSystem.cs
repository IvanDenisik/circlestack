using Entitas;
using System.Collections.Generic;
using Common;
using DG.Tweening;
using GameStage;
using UnityEngine;

namespace CameraMechanic
{
    public class MoveCameraOnGameFinishedSystem : ReactiveSystem<GameEntity>
    {
        public MoveCameraOnGameFinishedSystem() : base(Contexts.sharedInstance.game)
        {

        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.GameStage);
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasGameStage && entity.gameStage.Stage == EnumGameStage.WaitingForRestart;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            var settings = Core.Helper.cameraSettings;
            var distance = Core.Context.stackParameters.CirclesCount * settings.moveCameraAwayDeltaPerStackHeight;
            var newCameraPosZ = Camera.main.transform.position.z - distance;
            Camera.main.transform.DOMoveZ(newCameraPosZ, settings.moveCameraAwayTime);
        }
    }
}