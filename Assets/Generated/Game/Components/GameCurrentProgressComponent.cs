//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentContextApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameContext {

    public GameEntity currentProgressEntity { get { return GetGroup(GameMatcher.CurrentProgress).GetSingleEntity(); } }
    public Progress.CurrentProgressComponent currentProgress { get { return currentProgressEntity.currentProgress; } }
    public bool hasCurrentProgress { get { return currentProgressEntity != null; } }

    public GameEntity SetCurrentProgress(int newHeight) {
        if (hasCurrentProgress) {
            throw new Entitas.EntitasException("Could not set CurrentProgress!\n" + this + " already has an entity with Progress.CurrentProgressComponent!",
                "You should check if the context already has a currentProgressEntity before setting it or use context.ReplaceCurrentProgress().");
        }
        var entity = CreateEntity();
        entity.AddCurrentProgress(newHeight);
        return entity;
    }

    public void ReplaceCurrentProgress(int newHeight) {
        var entity = currentProgressEntity;
        if (entity == null) {
            entity = SetCurrentProgress(newHeight);
        } else {
            entity.ReplaceCurrentProgress(newHeight);
        }
    }

    public void RemoveCurrentProgress() {
        currentProgressEntity.Destroy();
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public Progress.CurrentProgressComponent currentProgress { get { return (Progress.CurrentProgressComponent)GetComponent(GameComponentsLookup.CurrentProgress); } }
    public bool hasCurrentProgress { get { return HasComponent(GameComponentsLookup.CurrentProgress); } }

    public void AddCurrentProgress(int newHeight) {
        var index = GameComponentsLookup.CurrentProgress;
        var component = (Progress.CurrentProgressComponent)CreateComponent(index, typeof(Progress.CurrentProgressComponent));
        component.Height = newHeight;
        AddComponent(index, component);
    }

    public void ReplaceCurrentProgress(int newHeight) {
        var index = GameComponentsLookup.CurrentProgress;
        var component = (Progress.CurrentProgressComponent)CreateComponent(index, typeof(Progress.CurrentProgressComponent));
        component.Height = newHeight;
        ReplaceComponent(index, component);
    }

    public void RemoveCurrentProgress() {
        RemoveComponent(GameComponentsLookup.CurrentProgress);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherCurrentProgress;

    public static Entitas.IMatcher<GameEntity> CurrentProgress {
        get {
            if (_matcherCurrentProgress == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.CurrentProgress);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherCurrentProgress = matcher;
            }

            return _matcherCurrentProgress;
        }
    }
}
